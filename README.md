# README #

### What is this repository for? ###

* Teste para vaga de desenvolvedor back-end na Zup
* V1.0

### How do I get set up? ###

* Git clone no repositório
* Necessário Node e mongoDb instalado para rodar a aplicação;
* executar npm install para instalar as dependencias do projeto;
* iniciar mongoDb (URI configurada no projeto: "mongodb://localhost:27017/xy-inc", caso necessario alterar no arquivo config/database.js);
* npm start para executar o aplicativo;
* Utilize o postman para testar;
* Adicionar POI -> "http://localhost:3000/api/coordinate" - metodo: POST - body json -> { "name": "Teste", "coordinateX": 0, "coordinateY": 0 }
* Listar todos os POIs -> "http://localhost:3000/api/coordinate" - metodo: GET
* Listar os POIs por proximidade -> "http://localhost:3000/api/coordinate/20/10" - metodo: GET

### Who do I talk to? ###

* Kadu Artur Prussek
* kadu.artur@gmail.com | 047 98917-2809