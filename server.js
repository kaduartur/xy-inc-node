var http = require('http'),
    app = require('./config/express');
    require('./config/database');

var port = process.env.PORT || 3000;

http.createServer(app)
    .listen(port, () => console.log(`Server started on port ${port}`));