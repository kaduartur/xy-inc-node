var mongoose = require('mongoose');

var schema = mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    coordinateX: {
        type: Number,
        required: true,
        min: [0, 'Coordenadas não pode ser numero negativo']
    },
    coordinateY: {
        type: Number,
        required: true,
        min: [0, 'Coordenadas não pode ser numero negativo']
    }
});

mongoose.model('coordinate', schema);