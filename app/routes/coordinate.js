module.exports = function (app) {
    var api = app.api.coordinate;

    app.route('/api/coordinate')
        .get(api.getAll)
        .post(api.create);

    app.route('/api/coordinate/:coordinateX/:coordinateY')
        .get(api.getPointsInterestByProximity);
}