var mongoose = require('mongoose');

module.exports = function () {

    var api = {},
        model = mongoose.model('coordinate');

    api.create = function (req, res) {

        var coord = req.body;

        model.create(coord)
            .then(coordinate => res.status(200).json(coordinate))
            .catch(err => {
                console.log(err);
                res.status(500).json(err);
            });

    };

    api.getAll = function (req, res) {

        model.find()
            .then(coordinates => res.status(200).json(coordinates))
            .catch(err => {
                console.log(err);
                res.status(500).json(err);
            });

    };

    api.getPointsInterestByProximity = function (req, res) {

        var x = req.params.coordinateX,
            y = req.params.coordinateY,
            maxDistance = x - y,
            coordinatesFiltered = [];

        model.find()
            .then(coordinates => {

                if (coordinates.length > 0) {
                    for (var i in coordinates) {
                        if (coordinates.hasOwnProperty(i)) {
                            var a = Math.abs(coordinates[i].coordinateX - x);
                            var b = Math.abs(coordinates[i].coordinateY - y);

                            var total = a + b;

                            if (total <= maxDistance) {
                                coordinatesFiltered.push(coordinates[i]);
                            }
                        }
                    }

                    res.status(200).json(coordinatesFiltered);
                    return;
                } else {
                    res.status(500).send('Sem pontos de interesse cadastrados');
                }
            })
            .catch(err => {
                console.log(err);
                res.status(500).json(err);
            });

    };

    return api;

}