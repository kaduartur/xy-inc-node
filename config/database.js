var mongoose = require('mongoose');

const uri = 'mongodb://localhost:27017/xy-inc';

mongoose.connect(uri);

mongoose.connection
    .on('connected', () => console.log('MongoDB Connected'));


mongoose.connection
    .on('error', err => console.log(`Connection error: ${err}`));


mongoose.connection
    .on('disconnected', () => console.log('MongoDB disconnected'));

process.on('SIGINT', () => {
    mongoose.connection
        .close(() => {
            console.log('connection closed');
            process.exit();
        })
});